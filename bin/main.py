# -*- coding: utf-8 -*-
#------------------------------------------------------------------------------
# main.py -
#------------------------------------------------------------------------------
# Copyright (c) 2015 script dialogue,  All Rights Reserved.
#------------------------------------------------------------------------------

import sys
import os
import json
import time
import copy
import string
from collections import defaultdict
from functools import partial

import kivy.app
import kivy.uix.floatlayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.checkbox import CheckBox
from kivy.factory import Factory
from kivy.uix.scrollview import ScrollView
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.screenmanager import  Screen, ScreenManager
from kivy.uix.popup import Popup
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.uix.dropdown import DropDown
import mako.template
from modules.pdfParser.parser import parsePdf
import subprocess

kColorTable = {'Red'   :(1,0.75,0.75,1),
               'Green' :(0.75,1,0.75,1),
               'Blue'  :(0.75,0.9,1,1),
               'Yellow':(1,1,0.75,1),
               '_FT_'  :(1,0,0,1)}

def getLastPath(load=False, save=False):
    if not load and not save:
        return ""
    content = {}
    exeFolder = os.path.dirname(sys.argv[0])
    jsonPrefs = "/".join([exeFolder, 'preferences.json'])
    if os.path.exists(jsonPrefs):
        with open(jsonPrefs, 'r') as readFile:
            content = json.load(readFile)
    loadPath = content.get('load', os.path.expanduser("~"))
    if not os.path.exists(loadPath):
        loadPath = os.path.expanduser("~")
    savePath = content.get('save', os.path.expanduser("~"))
    if not os.path.exists(savePath):
        savePath = os.path.expanduser("~")
    if load:
        return loadPath
    elif save:
        return savePath

def cache_keys(save=False, load=False, email=None, keys=None):
    exeFolder = os.path.dirname(sys.argv[0])
    jsonPrefs = os.path.join(exeFolder, 'preferences.json')
    if not os.path.exists(jsonPrefs):
        with open(jsonPrefs, 'w') as _file:
            json.dump({}, _file)
    if save:
        with open(jsonPrefs, 'r') as _file:
            data = json.load(_file)
        with open(jsonPrefs, 'w') as _file:
            data.update(dict(license=dict(email=email,
                                          key=keys)))
            json.dump(data, _file, indent=4)
        return
    if load:
        with open(jsonPrefs, 'r') as _file:
            data = json.load(_file)
        return data.get('license')

def verify_email_key(email, usr_key):
    email = email.lower().strip()
    usr_key = usr_key.strip()
    key_path = '%s/_gen/key.sh' % os.path.dirname(sys.argv[0])
    process = subprocess.Popen([key_path, email], stdout=subprocess.PIPE)
    key, err = process.communicate()
    if err:
        raise Exception("Failed to validate license due to %s" % err)
    if key.strip() == usr_key:
        return True
    else:
        raise Exception('Wrong email or key')

class LoadDialog(kivy.uix.floatlayout.FloatLayout):
    load = ObjectProperty(None)
    save = ObjectProperty(None)
    cancel = ObjectProperty(None)
    path = StringProperty("/")
    fileChooser = ObjectProperty(None)


class SaveDialog(kivy.uix.floatlayout.FloatLayout):
    save = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)
    path = StringProperty("/")

class NoteWindow(kivy.uix.floatlayout.FloatLayout):
    text_input = ObjectProperty(None)
    onEnter = ObjectProperty(None)

class MessageBox(kivy.uix.floatlayout.FloatLayout):
    okEnter = ObjectProperty(None)
    message = StringProperty("")

class LabelBG(Label):
    pass

class DoubleClickButton(Button):
    doubleClickCallback = ObjectProperty(None)
    def on_touch_down(self, touch):
        if touch.is_double_tap:
            self.doubleClickCallback()
        super(DoubleClickButton, self).on_touch_down(touch)

class CheckBoxBG(CheckBox):
    pass

class PdfButton(Button):
    exeFolder = os.path.dirname(sys.argv[0])
    callback = ObjectProperty(None)

class CheckBoxLabel(BoxLayout):
    text = StringProperty("")
    label = ObjectProperty(None)
    checkBox = ObjectProperty(None)

class EditScreen(Screen):
    masterLayout = ObjectProperty(None)
    exeFolder = os.path.dirname(sys.argv[0])
    applyEdit = ObjectProperty(None)

class EULA(Screen):
    submit = ObjectProperty(None)
    exeFolder = os.path.dirname(sys.argv[0])

    def __init__(self, **kwargs):
        self.startScreen = None
        self.user = kwargs.get('user')
        self.pwd = kwargs.get('pwd')
        self.cached = kwargs.get('cached')
        super(EULA, self).__init__(**kwargs)

    def loadStartScreen(self):
        self.startScreen = StartScreen(self.user, name='startScreen')
        self.manager.add_widget(self.startScreen)
        self.manager.current = 'startScreen'
        if not self.cached:
            cache_keys(True, False, self.user, self.pwd)

class LicenseWindow(Screen):
    email_input = ObjectProperty(None)
    key_input = ObjectProperty(None)
    submit = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.startEula = None
        super(LicenseWindow, self).__init__(**kwargs)

    def load_next_screen(self):
        try:
            if verify_email_key(self.email_input.text, self.key_input.text):
                self.loadEula(self.email_input.text, self.key_input.text)
        except Exception as e:
            self.errorMessage(e)

    def loadEula(self, user, pwd):
        self.startEula = EULA(name='eula', user=user, pwd=pwd)
        self.manager.add_widget(self.startEula)
        self.manager.current = 'eula'

    def errorMessage(self, error):
        content = MessageBox(message=str(error), okEnter= self.dismiss_popup)
        self._popup = Popup(title="Error", content=content, size_hint=(0.5, 0.25))
        self._popup.open()

    def dismiss_popup(self):
        self._popup.dismiss()

class StartScreen(Screen):
    license_info = StringProperty()
    def __init__(self, email, **kwargs):
        self.exeFolder = os.path.dirname(sys.argv[0])
        super(StartScreen, self).__init__(**kwargs)
        self.homeScreen = None
        self.libraryScreen = None
        self._popup = None
        self.showEula = None
        self.email = email
        self.license_info = "[color=000][size=35]This copy of Smart Dx is licensed to \n%s[/size][/color]" % email
        self.libraryData = "%s/assets/.libraryData.json"%self.exeFolder

    def nothing(self, *args, **kwargs):
        return

    def showLoad(self):
        content = LoadDialog(save=self.nothing, load=self.loadHome, cancel=self.dismiss_popup, path=getLastPath(True, False))
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def showLibrary(self):
        if not self.libraryScreen is None:
            self.libraryScreen.grid_l.clear_widgets()
            pass
        else:
            self.libraryScreen = LibraryScreen(name='libraryScreen', startScreen=self)
            self.manager.add_widget(self.libraryScreen)
        self.manager.current = 'libraryScreen'
        self.libraryScreen.loadData(self.libraryData)

    def loadHome(self, path, filename):
        if not self.homeScreen is None:
            self.homeScreen.grid_l.clear_widgets()
        else:
            self.homeScreen = HomeScreen(name='homeScreen')
            self.manager.add_widget(self.homeScreen)
        self.manager.current = 'homeScreen'
        self.homeScreen.load(path, filename)
        if self._popup:
            self.dismiss_popup()

    def tutorials(self):
        import webbrowser
        webbrowser.open("http://postfifthpictures.com/smart-dx-tutorials/")

    def loadEula(self):
        self.showEula = EULA(name='eula', user=self.email, cached=True)
        self.manager.add_widget(self.showEula)
        self.manager.current = 'eula'

    def dismiss_popup(self):
        self._popup.dismiss()

class LibraryScreen(Screen):
    grid_l = ObjectProperty(None)
    startScreen = ObjectProperty(None)
    editScreen = ObjectProperty(None)
    jsonData = ""
    _popup = None
    def loadData(self, jsonData):
        self.jsonData = jsonData
        if not os.path.exists(jsonData):
            return
        with open(jsonData, 'r') as _file:
            content = json.load(_file)
        self.loadPdf(None, content.values())

    def clearLibrary(self):
        self.grid_l.clear_widgets()
        with open(self.jsonData, 'w') as _file:
            json.dump({}, _file)

    def nothing(self, *args, **kwargs):
        return

    def showLoad(self):
        content = LoadDialog(save=self.nothing, load=self.loadPdf, cancel=self.dismiss_popup, path=getLastPath(True, False))
        content.fileChooser.multiselect = True
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def loadPdf(self, path, fileNames):
        pdfData = {}
        if os.path.exists(self.jsonData):
            with open(self.jsonData, 'r') as readData:
                pdfData = json.load(readData)
        if not isinstance(pdfData, dict):
            pdfData = {}
        for _file in fileNames:
            pdfData[os.path.basename(_file)] = _file
            self.grid_l.add_widget(PdfButton(text=os.path.basename(_file),
                                             callback=partial(self.startScreen.loadHome, os.path.dirname(_file),[_file])))
        with open(self.jsonData, 'w') as writeData:
            json.dump(pdfData, writeData, indent=4)
        self.dismiss_popup()

    def dismiss_popup(self):
        if self._popup:
            self._popup.dismiss()


class HomeScreen(Screen):
    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    editScreen = ObjectProperty(None)
    timeCode_l = ObjectProperty()
    grid_l = ObjectProperty(None)
    playIcon = ObjectProperty(None)
    filterButton = ObjectProperty(None)
    mainLayout = ObjectProperty(None)
    secsLabel = StringProperty("00:00:00:00")
    exeFolder = StringProperty(os.path.dirname(sys.argv[0]))

    def __init__(self, *args, **kwargs):
        super(HomeScreen, self).__init__(*args, **kwargs)
        self.frame = 0
        self.lastFrame = 0
        self.eventSecs = None
        self.exeFolder = os.path.dirname(sys.argv[0])
        self.jsonPrefs = "/".join([self.exeFolder, 'preferences.json'])
        self.loadPath = "/"
        self.savePath = "/"
        self.storedTimecode = None
        self.allData = []
        self.filterChecked = set()
        self.dropdown = DropDown()
        self.markerList = []
        self.takeCache = defaultdict(int)

    def openDropDown(self, *args, **kwargs):
        self.dropdown.open(self.filterButton)

    def dismiss_popup(self):
        self._popup.dismiss()

    def editMode(self):
        if not self.editScreen:
            self.editScreen = EditScreen(name="editScreen", applyEdit=self.applyEdit)
            self.manager.add_widget(self.editScreen)
            self.editScreen.gridLayout = GridLayout(cols=1, size_hint_y=None)
            self.editScreen.gridLayout.bind(minimum_height=self.editScreen.gridLayout.setter('height'))
            self.editScreen.scrollView = ScrollView(bar_width="8dp")
            self.editScreen.scrollView.add_widget(self.editScreen.gridLayout)
            self.editScreen.masterLayout.add_widget(self.editScreen.scrollView)
        self.editScreen.gridLayout.clear_widgets()
        for child in reversed(self.grid_l.children):
            textField = TextInput(text=child.text, size_hint= (1, None))
            textField.text_size = (self.width-20, None)
            textField.halign = 'left'
            textField.valign = 'middle'
            textField.bind(width=self.setBtnWidth)
            self.editScreen.gridLayout.add_widget(textField)
        self.manager.current = 'editScreen'

    def applyEdit(self, gridLayout):
        for child in reversed(gridLayout.children):
            newText = child.text
            newText = newText.decode('ascii', 'ignore')
            try:
                textParts = newText.split(":  ")
                prefix = textParts[0]
                newMsg = ":  ".join(textParts[1:])
                for data in self.allData:
                    if prefix == data.get('char'):
                        data['dialogue'] = newMsg
            except Exception, e:
                Logger.warning(e)
                pass
        try:
            self.applyFilter()
        except Exception, e:
            Logger.warning(e)
        self.manager.current = 'homeScreen'


    def applyFilter(self):
        grid = self.grid_l
        grid.clear_widgets()
        if len(self.filterChecked) == 0:
            for data in self.allData:
                btn = self.addButton(grid, data.get('char'), data.get('dialogue'))
                if data.get('color'):
                    btn.background_color = data.get('color')
                grid.add_widget(btn)
            return
        for data in self.allData:
            if data.get('char').split(". ")[1] in self.filterChecked:
                btn = self.addButton(grid, data.get('char'), data.get('dialogue'))
                if data.get('color'):
                    btn.background_color = data.get('color')
                grid.add_widget(btn)

    def addButton(self, grid, key, value):
        btn1 = Button(size_hint=(1, None))
        try:
            btn1.text = '%s:  %s' % (key.encode('utf8'), value.encode('utf8'))
        except:
            print locals()
        btn1.text_size = (self.width-20, None)
        btn1.halign = 'left'
        btn1.valign = 'middle'
        btn1.bind(on_release=partial(self.btn1Pressed, key, value))
        grid.bind(minimum_height=btn1.setter('height'),
                  minimum_width=btn1.setter('width'))
        btn1.bind(size=self.setBtnSize)
        return btn1

    def editTimecode(self):
        content = NoteWindow(onEnter=self.setTimecode)
        content.text_input.text = self.secsLabel
        content.text_input.font_size = 80
        self._popup = Popup(title="Edit Timecode", content=content, size_hint=(1, None), height=220)
        self._popup.open()

    def setTimecode(self, text):
        self.secsLabel = text
        self.lastFrame = self.frame = self.timecodeToFrames(self.secsLabel)
        self._popup.dismiss()

    def showSave(self):
        self.stopTimeCode()
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup, path=getLastPath(False, True))
        self._popup = Popup(title="Save file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def openNote(self):
        self.storedTimecode = self.secsLabel
        content = NoteWindow(onEnter=self.onEnter)
        self._popup = Popup(title="Note", content=content, size_hint=(1, 0.5))
        self._popup.open()

    def onEnter(self, text):
        self.addMarker("SmartDx", text, 'Red')
        self.dismiss_popup()

    def stopTimeCode(self):
        Clock.unschedule(self.eventSecs)
        self.eventSecs = None
        self.lastFrame = copy.copy(self.frame)
        self.playIcon.source = '%s/assets/play.png'%self.exeFolder

    def load(self, path, filename):
        self.updateLastPath('load', path)
        if not filename:
            self.dismiss_popup()
            return
        grid = self.grid_l
        grid.bind(minimum_height=grid.setter('height'),
                  minimum_width=grid.setter('width'))

        pdfData = parsePdf(filename[0])
        filterSet = set()
        for index, (key, value) in enumerate(pdfData.items()):
            lineNumber = value.get('lineNumber')
            if not lineNumber:
                lineNumber = index + 1
            key = "%d. %s"%(int(lineNumber), key.split("_")[0])
            filterSet.add(key.split(". ")[1])
            btn1 = Button(size_hint=(1, None))
            btn1.text = '%s:  %s' % (key, value.get('dialogue'))
            btn1.text_size = (self.width-20, btn1.height)
            btn1.halign = 'left'
            btn1.valign = 'middle'
            btn1.text_size = (self.width - 20, None)
            if btn1.texture_size[-1] > 100:
                btn1.height = btn1.texture_size[-1]
            else:
                btn1.height = 100
            grid.bind(minimum_height=btn1.setter('height'),
                      minimum_width=btn1.setter('width'))
            btn1.bind(size=self.setBtnSize)
            btn1.bind(on_release=partial(self.btn1Pressed, key, value.get("dialogue")))
            grid.add_widget(btn1)
            self.allData.append({'char':key,
                                 'dialogue':value.get('dialogue')})
        self.dropdown.clear_widgets()
        for filterItem in filterSet:
            checkBox = CheckBoxLabel(text='%s' % filterItem, size_hint= (1, None))
            checkBox.label.text_size = (self.width-20, None)
            checkBox.label.halign = 'left'
            checkBox.label.bind(width=self.setBtnWidth)
            checkBox.checkBox.size_hint = (None, 1)
            checkBox.checkBox.bind(active=partial(self.filterItem, checkBox.label.text))
            self.dropdown.add_widget(checkBox)

    def filterItem(self, labelName, instance, state):
        if state:
            self.filterChecked.add(labelName)
            self.applyFilter()
        else:
            self.filterChecked.remove(labelName)
            self.applyFilter()

    def addFrame(self, number):
        self.lastFrame += number
        self.frame += number
        self.secsLabel = self.getTimecode(self.frame)

    def setBtnWidth(self, instance, value):
        instance.text_size = (value - 20, None)

    def setBtnSize(self, instance, value):
        instance.text_size = (value[0] - 20, None)
        if instance.texture_size[-1] > 100:
            instance.height = instance.texture_size[-1] + 10
        else:
            instance.height = 100

    def btn1Pressed(self, key, value, instance, *args):
        self.checkTimeCode()
        self.addMarker(key, value, 'Red', instance)

    def addMarker(self, name, dialogue, color, instance=None):
        self.checkTimeCode()
        lineNumber = name.split(".")[0]
        timecode = self.secsLabel
        if not self.storedTimecode is None:
            timecode = self.storedTimecode
        self.takeCache[name] += 1
        self.markerList.append({'name':"SmartDx", 'dialogue':dialogue,
                                'lineNumber':lineNumber, 'timecode':timecode,
                                'color':color, 'take': "TK%02d"%self.takeCache[name]})
        self.storedTimecode = None
        if instance is not None:
            instance.background_color = kColorTable[color]
            self.updateDataColor(name, color)

    def updateDataColor(self, name, color):
        dataItem = [data for data in self.allData if data.get('char') == name]
        if len(dataItem) == 1:
            dataItem[0]['color'] = kColorTable[color]

    def updateMarker(self, color):
        if len(self.markerList) >= 1:
            lastItem = self.markerList[len(self.markerList)-1]
            if color == "_FT_":
                lastItem['take'] = "FT"
            else:
                lastItem['color'] = color
            try:
                lastWidget = (x for x in self.grid_l.children if x.text.split(".")[0] == lastItem['lineNumber']).next()
                lastWidget.background_color = kColorTable[color]
                self.updateDataColor(lastItem['dialogue'].split(":  ")[0], color)
            except StopIteration:
                Logger.warning("Failed to get the last modified marker...")

    def _incrementSeconds(self, dt):
        frame = (time.time()-self.startTime)*24.0
        self.frame = int(round(frame)) + self.lastFrame
        self.secsLabel = self.getTimecode(self.frame)

    def getTimecode(self, frame):
        return '{0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(frame / (3600*24),
                                                        frame / (60*24) % 60,
                                                        frame / 24 % 60,
                                                        frame % 24)
    def timecodeToFrames(self, timecode):
        return sum(f * int(t) for f,t in zip((3600*24, 60*24, 24, 1), timecode.split(':')))

    def startTimecode(self, instance, *args):
        if not self.eventSecs is None:
            self.stopTimeCode()
            self.playIcon.source = '%s/assets/play.png'%self.exeFolder
        else:
            self.checkTimeCode()
            self.playIcon.source = '%s/assets/stop.png'%self.exeFolder

    def checkTimeCode(self):
        if self.eventSecs is None:
            self.startTime = time.time()
            realFps = float(Clock.get_rfps())
            appFps = float(Clock.get_fps())
            fps = 24.0*(appFps/realFps)
            self.eventSecs = Clock.schedule_interval(self._incrementSeconds, 1.0/fps)
            self.playIcon.source = '%s/assets/stop.png'%self.exeFolder

    def updateLastPath(self, prefType, path):
        content = {}
        if os.path.exists(self.jsonPrefs):
            with open(self.jsonPrefs, 'r') as readFile:
                content = json.load(readFile)
        content.update({prefType:path})
        with open(self.jsonPrefs, 'w') as _file:
            json.dump(content, _file)
        lastPathAttr = getattr(self, "%sPath"%prefType, None)
        if lastPathAttr is not None:
            setattr(self, "%sPath"%prefType, path)

    def save(self, path, filename):
        self.updateLastPath('save', path)
        content = None
        if not os.path.exists(os.path.dirname(filename)):
            filePath = "/".join([path, filename])
            if not filePath.endswith(".txt"):
                filePath += ".txt"
        else:
            filePath = filename
        templatePath = "%s/templates/avidMarkers.mako"%self.exeFolder
        args = {'markers':self.markerList}
        with open(templatePath, 'r') as templateFile:
            content = mako.template.Template(templateFile.read(), default_filters=['decode.utf8'], input_encoding='utf-8', output_encoding='utf-8').render(**args)
        if not content is None:
            with open(filePath, 'w') as file_:
                file_.write(content)
        self.dismiss_popup()
        self.successMessage(filePath)

    def successMessage(self, filePath):
        content = MessageBox(message="Your markers are saved to\n%s"%filePath, okEnter= self.dismiss_popup)
        self._popup = Popup(title="Complete", content=content, size_hint=(0.5, 0.25))
        self._popup.open()


class SmartDX(kivy.app.App):
    icon = "%s/assets/dialogue.png"%os.path.dirname(sys.argv[0])
    title="SmartDx (0.1.5)"
    def build(self):
        sm = ScreenManager()
        cached_license = cache_keys(load=True)
        if cached_license:
            try:
                if verify_email_key(cached_license.get('email'), cached_license.get('key')):
                    sm.add_widget(StartScreen(cached_license.get('email'), name='startScreen'))
                    return sm
            except:
                pass
        sm.add_widget(LicenseWindow(name='licenseWindow'))
        return sm


Factory.register('HomeScreen', cls=HomeScreen)
Factory.register('LoadDialog', cls=LoadDialog)
Factory.register('SaveDialog', cls=SaveDialog)


if __name__ == '__main__':
    SmartDX().run()