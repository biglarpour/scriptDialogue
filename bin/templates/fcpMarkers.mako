<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xmeml>
<xmeml version="4">
    <sequence id="${projectName}" TL.SQAudioVisibleBase="0" TL.SQVideoVisibleBase="0" TL.SQVisibleBaseTime="0" TL.SQAVDividerPosition="0.5" TL.SQHideShyTracks="0" TL.SQHeaderWidth="168" Monitor.ProgramZoomOut="10603059667200" Monitor.ProgramZoomIn="0" TL.SQTimePerPixel="0.20000000000000001110223024625" MZ.EditLine="1203544742400" MZ.Sequence.PreviewFrameSizeHeight="540" MZ.Sequence.PreviewFrameSizeWidth="960" MZ.Sequence.AudioTimeDisplayFormat="200" MZ.Sequence.PreviewRenderingClassID="1297106761" MZ.Sequence.PreviewRenderingPresetCodec="1297107278" MZ.Sequence.PreviewRenderingPresetPath="EncoderPresets/SequencePreview/795454d9-d3c2-429d-9474-923ab13b7018/I-Frame Only MPEG.epr" MZ.Sequence.PreviewUseMaxRenderQuality="false" MZ.Sequence.PreviewUseMaxBitDepth="false" MZ.Sequence.EditingModeGUID="795454d9-d3c2-429d-9474-923ab13b7018" MZ.Sequence.VideoTimeDisplayFormat="112" MZ.WorkOutPoint="10603056492000" MZ.WorkInPoint="0" MZ.ZeroPoint="0">
        <duration>${duration}</duration>
        <rate>
            <timebase>24</timebase>
            <ntsc>FALSE</ntsc>
        </rate>
        <name>${projectName}</name>
        % for marker in markers:
            <marker>
                <comment>${marker['dialogue']}</comment>
                <name>${marker['name']}</name>
                <in>${marker['in']}</in>
                <out>-1</out>
            </marker>
        % endfor
    </sequence>
</xmeml>