#!/usr/bin/env bash
if [ -z "$1" ]
  then
    echo "No Email Supplied"
    exit 64
fi
if [[ "$1" == *"@"* && "$1" == *"."* ]]; then
  email=$(echo "$1" | awk '{print tolower($0);}' )
  echo -n "smart$email+Dx" | openssl sha1
  exit 0
fi
echo "Invalid Email Supplied"
exit 64
