# Written by Mathieu Virbel
# Taken from https://gist.github.com/tito/5116183

from kivy.clock import Clock

class _async_generator(object):
    _calls = []

    def __init__(self, func, args, callback):
        object.__init__(self)
        _async_generator._calls.append(self)
        self.func = func
        self.args = args
        self.callback = callback
        self.gen = func(*args)
        self.pump()

    def stop(self):
        try:
            if self.gen:
                self.gen.send(True)
        except StopIteration:
            pass
        self._end()

    def pump(self, *args):
        gen = self.gen
        if gen is not None:
            try:
                ret = gen.next()
                if ret != None:
                    Clock.schedule_once(self.pump, ret)
                    return
            except StopIteration:
                pass
        self._end()

    def _end(self):
        if self not in _async_generator._calls:
            return
        if self.callback:
            self.callback(*self.args)
        _async_generator._calls.remove(self)
        Clock.unschedule(self.pump)

class asynchronous(object):
    def __init__(self, callback=None):
        super(asynchronous, self).__init__()
        self.callback = callback

    def __call__(self, func):
        def wrapped_f(*args):
            return _async_generator(func, args, self.callback)
        return wrapped_f
