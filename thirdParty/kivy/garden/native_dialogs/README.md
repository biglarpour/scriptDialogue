Native dialogs for Kivy
=======================

Use OS native dialogs within Kivy applications to open files, etc.
Currently it supports only Zenity and KDialog (on GNU-based OSes),
but support for Android, Windows and Mac OS X is planned.

## Usage

```
from kivy.garden.native_dialogs import NativeFileChooser

# [...]
# in your app

chooser = NativeFileChooser()
chooser.bind(on_submit=self.my_callback)
chooser.open()
```

This module tries to imitate Kivy's FileChooser API, however some events/properties aren't available.
These is the subset of events and properties supported by NativeFileChooser (check Kivy's documentation for more details):

#### Properties:

* dirselect
* filters (partial, see below)
* multiselect
* path
* selection
* show_hidden (currently not supported by any of the existing back-ends)

#### Events:

* on_submit
* on_cancel

However, NativeFileChooser has a few additional properties that FileChooser doesn't have:

* **title (StringProperty):** the title of the file chooser window, or None for default.
* **icon (StringProperty):** the icon of the file chooser window, or None for default.
* **mode (OptionProperty):** either "open" or "save", changes the way the dialog behaves. Default is "open".
* **preview (BooleanProperty):** if supported, shows a preview of the selected file. Default is False.

Two methods are available: **open** and **dismiss**. They just do what they promise.

### Filters

Filters are supported, but in a slightly different way. You can **not** provide callables as filters, as they can't be passed to a subprocess. You can, however, provide string filters the same way you're used to (e.g. "*.mp3"), or group filters into a tuple. The first item of the tuple is the **name** of the filter (it will not be used as a filter), and all the following ones (you can put as many as you want) are the actual filters in that group. If the back-end supports filter grouping, they will be displayed as a single item in the filter selection menu.

## Licensing

This module is licensed under the MIT license.