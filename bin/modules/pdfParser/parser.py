#------------------------------------------------------------------------------
# parser.py -
#------------------------------------------------------------------------------
# Copyright (c) 2015 script dialogue,  All Rights Reserved.
#------------------------------------------------------------------------------

from pdfminer.layout import LAParams, LTTextBox, LTTextLine, LTFigure, LTTextLineHorizontal, LTTextBoxHorizontal, LTPage, LTChar, LTAnno

from pdfminer.pdfpage import PDFPage
from cStringIO import StringIO
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator
# from kivy.logger import Logger

from pdfminer.pdfdocument import PDFDocument, PDFNoOutlines
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import PDFPageAggregator

import collections

lineNumberRange = (0,100)
characterRange = (250, 300)

class PDFPageDetailedAggregator(PDFPageAggregator):
    def __init__(self, rsrcmgr, pageno=1, laparams=None):
        PDFPageAggregator.__init__(self, rsrcmgr, pageno=pageno, laparams=laparams)
        self.rows = []
        self.page_number = 0

    def receive_layout(self, ltpage):
        def render(item, page_number):
            if isinstance(item, LTPage) or isinstance(item, LTTextBox):
                for child in item:
                    render(child, page_number)
            elif isinstance(item, LTTextLine):
                child_str = ''
                for child in item:
                    if isinstance(child, (LTChar, LTAnno)):
                        child_str += child.get_text()
                child_str = ' '.join(child_str.split()).strip()
                if child_str:
                    # bbox == (x1, y1, x2, y2)
                    row = {"page_number": int(page_number),
                           "textX1": int(item.bbox[0]),
                           "textY1": int(item.bbox[1]),
                           "textX2": int(item.bbox[2]),
                           "textY2": int(item.bbox[3]),
                           "dialogue": child_str}
                    self.rows.append(row)
                for child in item:
                    render(child, page_number)
            return
        render(ltpage, self.page_number)
        self.page_number += 1
        self.rows = sorted(self.rows, key = lambda x: (x["page_number"], -x['textY1']))
        self.result = ltpage

def parsePdf(path):
    with open(path,'rb') as fp:

        parser = PDFParser(fp)
        doc = PDFDocument(parser)

        rsrcmgr = PDFResourceManager()
        laparams = LAParams()
        device = PDFPageDetailedAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)

        for page in PDFPage.create_pages(doc):
            interpreter.process_page(page)
            # receive the LTPage object for this page
            device.get_result()

        lines = device.rows
        textDict = collections.OrderedDict()
        charName = None
        line_number = None
        textY = 0
        for index, line in enumerate(lines):
            textX = line['textX1']
            line_num_bool = (lineNumberRange[0] < textX < lineNumberRange[1] and line['dialogue'].strip().isdigit())
            if characterRange[0] <= textX <= characterRange[1]:
                charName = line["dialogue"].strip() + "_%s"%index
                textDict[charName] = {'dialogue':"",
                                      'lineNumber': line_number if isinstance(line_number, int) else "",
                                      'pageNumber': line['page_number']}
                textY = line['textY1']
                continue
            if charName and 10 < (textY - line['textY1']) < 24 and \
                    textX > 60 and  textDict[charName]["pageNumber"] == line['page_number']:
                textDict[charName]['dialogue'] += " %s"%line['dialogue'].strip()
                textY = line['textY1']
                continue
            elif charName and not textDict[charName]['lineNumber'] and \
                    lineNumberRange[0] < textX < lineNumberRange[1] and \
                    line['dialogue'].strip().isdigit() and \
                    textDict[charName]["pageNumber"] == line['page_number']:
                textDict[charName]['lineNumber'] = int(line['dialogue'].strip())
            elif line_num_bool:
                line_number = int(line['dialogue'].strip())
            textY = line['textY1']
        return textDict

if __name__ == '__main__':
    parsePdf("/Development/scriptDialogue/bin/assets/sample_1.pdf")