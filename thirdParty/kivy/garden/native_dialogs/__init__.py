"""Use OS native dialogs within Kivy applications to open files, etc.
Currently it supports only Zenity and KDialog (on GNU-based OSes),
but support for Android, Windows and Mac OS X is planned.
"""

__all__ = ["NativeFileChooser", "NativeFileChooserBase",
           "SubprocessFileChooser", "ZenityFileChooser",
           "KDialogFileChooser", "get_de"]

import os
from async import asynchronous
from kivy import platform
from kivy.event import EventDispatcher
from kivy.properties import *
import subprocess as sp
import traceback
import threading
import inspect
import ctypes

if platform == "linux":
    from distutils.spawn import find_executable as which
elif platform == "win":
    from win32com.shell import shell, shellcon
    import win32gui, win32con, pywintypes
elif platform == "android":
    from jnius import autoclass

def get_de():
    if platform != "linux":
        return str(platform)
    else:
        if str(os.environ.get("XDG_CURRENT_DESKTOP")).lower() == "kde" and which("kdialog"):
            return "kde"
        elif which("yad"):
            return "yad"
        elif which("zenity"):
            return "gnome"

class NativeFileChooserBase(EventDispatcher):
    """Implementation of FileChooserController which uses native dialogs instead.
    This class does nothing, it needs to be implemented for specific platforms.

    Properties and methods which are also in Kivy's filechooser are not
    documented. Please refer to
    http://kivy.org/docs/api-kivy.uix.filechooser.html#kivy.uix.filechooser.FileChooserController
    for more details.

    This class contains only a small subset of the features implemented in
    Kivy's FileChooser, and this subset is not guaranteed to work the same
    way in all the supported platforms. Please look at each implementation's
    documentation for more details.
    """

    title = StringProperty(None, allownone=True)
    """The title of the file chooser dialog, if supported by the back-end,
    or None to use the default one.

    title is a StringProperty which defaults to None.
    """

    icon = StringProperty(None, allownone=True)
    """An icon for the dialog, if supported by the back-end, or None for the
    default.

    icon is a StringProperty which defaults to None.
    """

    mode = OptionProperty("open", options=("open", "save"))
    """This property is not found in Kivy as Kivy doesn't provide open/save
    dialogs/popups, only file choosers.

    If set to "open". the back-end will be asked to allow only existing files
    to be selected. If set to "save", the user will be able to specify
    non-existing paths and, if an existing file is selected, the back-end
    will likely ask the user for confirmation.

    This property is ignored when *dirselect* is set to True.

    mode is an OptionProperty which defaults to "open". Options are "open"
    and "save".
    """

    preview = BooleanProperty(False)
    """If supported, ask the back-end to show a preview of the selected file.

    preview is a BooleanProperty which defaults to False.
    """

    dirselect = BooleanProperty(False)

    filters = ListProperty([])
    """Please do not provide callback filters. Filtering is done by the back-end
    this module has no control on that.

    You can however provide one or more filters as a list of tuples, where the
    first item is a human readable name of the filter, and the following ones
    are the actual filters (for example [("Photos", "*jpg", "*JPG", "*JPEG")]).
    """

    multiselect = BooleanProperty(False)

    path = StringProperty(None, allownone=True)

    selection = ListProperty([])

    show_hidden = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.register_event_type('on_submit')
        self.register_event_type('on_cancel')
        super(NativeFileChooserBase, self).__init__(**kwargs)

    def _open(self, *args):
        raise NotImplementedError

    def open(self):
        """Opens the dialog. Beware that depending on the system, it might
        not be focused automatically.
        """
        self._open()


    def _dismiss(self, *args):
        raise NotImplementedError

    def dismiss(self):
        """Dismisses the dialog, if possible. This might not work on all
        platforms.
        """
        self._dismiss()

    def on_submit(self, selection):
        """Triggered when the user submits the selected files. A
        "selection" argument is also passed with the list of the
        selected files.
        """
        pass

    def on_cancel(self):
        """Triggered when the user closes the dialog without selecting
        anything.
        """
        pass


class SubprocessFileChooser(NativeFileChooserBase):
    """A NativeFileChooser semi-implementation that allows using
    subprocess back-ends.
    Normally you only need to override _gen_cmdline.
    """

    executable = StringProperty("")
    """The name of the executable of the back-end.

    executable is a StringProperty which defaults to an empty string.
    """

    separator = StringProperty("|")
    """The separator used by the back-end. Override this for automatic
    splitting, or override _split_output.

    separator is a StringProperty which defaults to "|".
    """

    successretcode = NumericProperty(0)
    """The return code which is returned when the user doesn't close the
    dialog without choosing anything, or when the app doesn't crash.

    successretcode is a NumericProperty which defaults to 0.
    """

    _process = ObjectProperty(None)

    @asynchronous()
    def _run_command(self, cmd):
        print "run cmd", cmd
        self._process = sp.Popen(cmd, stdout=sp.PIPE)
        while True:
            ret = self._process.poll()
            if ret == None:
                yield 0
                continue
            else:
                if ret == self.successretcode:
                    out = self._process.communicate()[0].strip()
                    self.selection = self._split_output(out)
                    self.dispatch("on_submit", self.selection)
                    break
                else:
                    self.dispatch("on_cancel")
                    break

    @asynchronous()
    def _kill(self, *args):
        count = 0
        while count < 10:
            count += 1
            if self._process.poll() != None:
                break
            try:
                self._process.terminate()
            except:
                traceback.print_exc()
            if self._process.poll() == None:
                yield 0.1
        else:
            try:
                self._process.kill
            except:
                traceback.print_exc()

    def _split_output(self, out):
        """This methods receives the output of the back-end and turns
        it into a list of paths.
        """
        return out.split(self.separator)

    def _gen_cmdline(self):
        """Returns the command line of the back-end, based on the current
        properties. You need to override this.
        """
        raise NotImplementedError()

    def _open(self):
        self._run_command(self._gen_cmdline())

    def _dismiss(self):
        self._kill()


class ZenityFileChooser(SubprocessFileChooser):
    """A NativeFileChooser implementation using Zenity (on GNU/Linux).

    Not implemented features:
    * show_hidden
    * preview
    """

    executable = "zenity"
    separator = "|"
    successretcode = 0

    def _gen_cmdline(self):
        cmdline = [which(self.executable), "--file-selection", "--confirm-overwrite"]
        if self.multiselect:
            cmdline += ["--multiple"]
        if self.mode == "save" and not self.dirselect:
            cmdline += ["--save"]
        if self.dirselect:
            cmdline += ["--directory"]
        if self.path:
            cmdline += ["--filename", self.path]
        if self.title:
             cmdline += ["--name", self.title]
        if self.icon:
            cmdline += ["--window-icon", self.icon]
        for f in self.filters:
            if type(f) == str:
                cmdline += ["--file-filter", f]
            else:
                cmdline += ["--file-filter", "{name} | {flt}".format(name=f[0], flt=" ".join(f[1:]))]
        return cmdline

class KDialogFileChooser(SubprocessFileChooser):
    """A NativeFileChooser implementation using KDialog (on GNU/Linux).

    Not implemented features:
    * show_hidden
    * preview
    """

    executable = "kdialog"
    separator = "\n"
    successretcode = 0

    def _gen_cmdline(self):
        cmdline = [which(self.executable)]

        filt = []

        for f in self.filters:
            if type(f) == str:
                filt += [f]
            else:
                filt += list(f[1:])

        if self.dirselect:
            cmdline += ["--getexistingdirectory", (self.path if self.path else os.path.expanduser("~"))]
        elif self.mode == "open":
            cmdline += ["--getopenfilename", (self.path if self.path else os.path.expanduser("~")), " ".join(filt)]
        elif self.mode == "save":
            cmdline += ["--getopenfilename", (self.path if self.path else os.path.expanduser("~")), " ".join(filt)]
        if self.multiselect:
            cmdline += ["--multiple", "--separate-output"]
        if self.title:
            cmdline += ["--title", self.title]
        if self.icon:
            cmdline += ["--icon", self.icon]
        return cmdline

class YADFileChooser(SubprocessFileChooser):
    """A NativeFileChooser implementation using YAD (on GNU/Linux).

    Not implemented features:
    * show_hidden
    """

    executable = "yad"
    separator = "|?|"
    successretcode = 0

    def _gen_cmdline(self):
        cmdline = [which(self.executable), "--file-selection", "--confirm-overwrite", "--geometry", "800x600+150+150"]
        if self.multiselect:
            cmdline += ["--multiple", "--separator", self.separator]
        if self.mode == "save" and not self.dirselect:
            cmdline += ["--save"]
        if self.dirselect:
            cmdline += ["--directory"]
        if self.preview:
            cmdline += ["--add-preview"]
        if self.path:
            cmdline += ["--filename", self.path]
        if self.title:
             cmdline += ["--name", self.title]
        if self.icon:
            cmdline += ["--window-icon", self.icon]
        for f in self.filters:
            if type(f) == str:
                cmdline += ["--file-filter", f]
            else:
                cmdline += ["--file-filter", "{name} | {flt}".format(name=f[0], flt=" ".join(f[1:]))]
        return cmdline

# From http://stackoverflow.com/a/325528
# Asynchronously raise an exception in the thread to stop it
def _async_raise(tid, exctype):
    '''Raises an exception in the threads with id tid'''
    if not inspect.isclass(exctype):
        raise TypeError("Only types can be raised (not instances)")
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid,
                                                  ctypes.py_object(exctype))
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        # "if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, 0)
        raise SystemError("PyThreadState_SetAsyncExc failed")

class Win32GetFileName(threading.Thread):
    def __init__(self, mode, path, title, filters, dirselect, multiselect, show_hidden, **kwargs):
        self.mode = mode
        self.path = path
        self.title = title
        self.filters = filters
        self.dirselect = dirselect
        self.multiselect = multiselect
        self.show_hidden = show_hidden
        self.selection = None
        self.fname = None
        self.customfilter = None
        self.flags = None
        self.finished = False
        super(Win32GetFileName, self).__init__(**kwargs)

    def run(self):
        try:
            if not self.dirselect:
                args = {}

                if self.path:
                    atgs["InitialDir"] = os.path.dirname(self.path)
                    args["File"] = os.path.splitext(os.path.dirname(self.path))[0]
                    args["DefExt"] = os.path.splitext(os.path.dirname(self.path))[1]
                args["Title"] = self.title if self.title else "Pick a file..."
                args["CustomFilter"] = 'Other file types\x00*.*\x00'
                args["FilterIndex"] = 1

                filters = ""
                for f in self.filters:
                    if type(f) == str:
                        filters += (f + "\x00") * 2
                    else:
                        filters += f[0] + "\x00" + ";".join(f[1:]) + "\x00"
                args["Filter"] = filters

                flags = win32con.OFN_EXTENSIONDIFFERENT | win32con.OFN_OVERWRITEPROMPT
                if self.multiselect:
                    flags |= win32con.OFN_ALLOWMULTISELECT | win32con.OFN_EXPLORER
                if self.show_hidden:
                    flags |= win32con.OFN_FORCESHOWHIDDEN
                args["Flags"] = flags

                if self.mode == "open":
                    self.fname, self.customfilter, self.flags = win32gui.GetOpenFileNameW(**args)
                elif self.mode == "save":
                    self.fname, self.customfilter, self.flags = win32gui.GetSaveFileNameW(**args)

                if self.fname:
                    if self.multiselect:
                        seq = str(self.fname).split("\x00")
                        dir_n, base_n = seq[0], seq[1:]
                        self.selection = [os.path.join(dir_n, i) for i in base_n]
                    else:
                        self.selection = str(self.fname).split("\x00")
            else:
                # From http://timgolden.me.uk/python/win32_how_do_i/browse-for-a-folder.html
                pidl, display_name, image_list = shell.SHBrowseForFolder(
                win32gui.GetDesktopWindow(), None,
                self.title if self.title else "Pick a folder...", 0, None, None)
                self.selection = [str(shell.SHGetPathFromIDList (pidl))]

            print self.selection
            self.finished = True
        except (RuntimeError, pywintypes.error):
            self.finished = True
            self.selection = None

    def poll(self):
        if not self.finished:
            return None
        else:
            return self.selection == None and 1 or 0

    # From http://stackoverflow.com/a/325528
    # Asynchronously raise an exception in the thread to stop it
    def _get_my_tid(self):
        """determines this (self's) thread id

        CAREFUL : this function is executed in the context of the caller
        thread, to get the identity of the thread represented by this
        instance.
        """
        if not self.isAlive():
            raise threading.ThreadError("the thread is not active")

        # do we have it cached?
        if hasattr(self, "_thread_id"):
            return self._thread_id

        # no, look for it in the _active dict
        for tid, tobj in threading._active.items():
            if tobj is self:
                self._thread_id = tid
                return tid

        # TODO: in python 2.6, there's a simpler way to do : self.ident

        raise AssertionError("could not determine the thread's id")

    def raise_exc(self, exctype):
        """Raises the given exception type in the context of this thread.

        If the thread is busy in a system call (time.sleep(),
        socket.accept(), ...), the exception is simply ignored.

        If you are sure that your exception should terminate the thread,
        one way to ensure that it works is:

            t = ThreadWithExc( ... )
            ...
            t.raise_exc( SomeException )
            while t.isAlive():
                time.sleep( 0.1 )
                t.raiseExc( SomeException )

        If the exception is to be caught by the thread, you need a way to
        check that your thread has caught it.

        CAREFUL : this function is executed in the context of the
        caller thread, to raise an excpetion in the context of the
        thread represented by this instance.
        """
        _async_raise( self._get_my_tid(), exctype )

class Win32FileChooser(NativeFileChooserBase):
    """A native implementation of NativeFileChooser using the
    Win32 API on Windows.

    Not Implemented features (general):
    * preview
    * window-icon
    * DISMISS (dialogs can't currently be dismissed by the application)

    Not implemented features (in directory selection only - it's limited
    by the OS itself):
    * preview
    * window-icon
    * multiselect
    * show_hidden
    * filters
    * path
    """

    _thread = ObjectProperty(None)

    @asynchronous()
    def _start_thread(self):
        self._thread = Win32GetFileName(self.mode, self.path, self.title, self.filters, self.dirselect, self.multiselect, self.show_hidden)

        self._thread.start()

        while True:
            ret = self._thread.poll()
            if ret == None:
                yield 0
                continue
            else:
                if ret == 0:
                    self.selection = self._thread.selection[:]
                    self.dispatch("on_submit", self.selection)
                    break
                else:
                    self.dispatch("on_cancel")
                    break

    def _kill_thread(self):
        ## FIXME: the exception is raised when the dialog is done
        if not self._thread.finished:
            # Raises an exception in self._thread
            self._thread.raise_exc(RuntimeError)

    def _open(self):
        self._start_thread()

    def _dismiss(self):
        self._kill_thread()

current_de = get_de()
choosers = {"gnome":     ZenityFileChooser,
            "kde":       KDialogFileChooser,
            "yad":       YADFileChooser,
            "win":       Win32FileChooser}

NativeFileChooser = choosers[current_de]

choosers["default"] = NativeFileChooser


# Support for running from the command line for testing

if __name__ == "__main__":
    from kivy.app import App
    from kivy.lang import Builder
    from kivy.uix.floatlayout import FloatLayout

    Builder.load_string("""
<Root>:
    GridLayout:
        orientation: "vertical"
        x: root.width / 2 - self.width / 2
        y: root.height / 2 - self.height / 2
        size_hint: None, None
        width: "400dp"
        height: self.minimum_height
        cols: 1
        spacing: "10dp"

        BoxLayout:
            orientation: "horizontal"
            size_hint_y: None
            height: "40dp"
            spacing: "10dp"

            ToggleButton:
                id: open
                text: "Open"
                state: "down"
                group: "mode"

            ToggleButton:
                id: save
                text: "Save"
                group: "mode"

            ToggleButton:
                id: dir
                text: "Directory"
                group: "mode"

        BoxLayout:
            orientation: "horizontal"
            size_hint_y: None
            height: "40dp"
            spacing: "10dp"

            ToggleButton:
                id: multiselect
                text: "Multi-select"

            ToggleButton:
                id: preview
                text: "Show preview\\n(YAD only)"

            ToggleButton:
                id: show_hidden
                text: "Show hidden\\n(Win only)"

        TextInput:
            id: w_title
            hint_text: "Window title (leave empty for default title)"
            size_hint_y: None
            height: "38dp"
            multiline: False
            on_text_validate: app.open_dialog("default")

        Widget:
            size_hint_y: None
            height: "5dp"

        Button:
            text: "Run default for platform"
            on_release: app.open_dialog("default")
            size_hint_y: None
            height: "40dp"

        Button:
            text: "Run YAD"
            on_release: app.open_dialog("yad")
            size_hint_y: None
            height: "40dp"

        Button:
            text: "Run Zenity"
            on_release: app.open_dialog("gnome")
            size_hint_y: None
            height: "40dp"

        Button:
            text: "Run KDialog"
            on_release: app.open_dialog("kde")
            size_hint_y: None
            height: "40dp"

        Button:
            text: "Run Windows file chooser"
            on_release: app.open_dialog("win")
            size_hint_y: None
            height: "40dp"

        Widget:
            size_hint_y: None
            height: "5dp"

        Button:
            text: "Dismiss (opening a new one will dismiss the open one)"
            on_release: app.dismiss_dialog()
            size_hint_y: None
            height: "40dp"

        Widget:
            size_hint_y: None
            height: "5dp"

        Label:
            id: selection
            size_hint_y: None
            height: "100dp"
            text_size: self.width, self.height
            text: "No files selected"
""")

    class Root(FloatLayout):
        pass

    class NativeDialogsApp(App):
        dialog = ObjectProperty(None, allownone=True)

        def build(self):
            return Root()

        def dismiss_dialog(self):
            if self.dialog:
                self.dialog.dismiss()
                self.dialog = None

        def open_dialog(self, dialog):
            self.dismiss_dialog()

            props = {"multiselect":   self.root.ids.multiselect.state == "down",
                     "mode":          self.root.ids.save.state == "down" and "save" or "open",
                     "dirselect":     self.root.ids.dir.state == "down",
                     "show_hidden":   self.root.ids.show_hidden.state == "down",
                     "preview":       self.root.ids.preview.state == "down",
                     "title":         self.root.ids.w_title.text if self.root.ids.w_title.text != "" else None}

            self.dialog = choosers[dialog](**props)
            self.dialog.bind(on_submit=self.read_selection, on_cancel=self.canceled)
            self.dialog.open()

        def read_selection(self, *args):
            self.root.ids.selection.text = str(self.dialog.selection)
            self.dialog = None

        def canceled(self, *args):
            self.dialog = None

    NativeDialogsApp().run()
